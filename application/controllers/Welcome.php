<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';
class Welcome extends CI_Controller {
	public function __construct()
	  {
	    parent::__construct();
      $this->load->library('email');
      $this->load->helper('url');
	  }

	public function index()
	{
		$this->load->view('index');
	}

	public function aviso(){
		$this->load->view('aviso-de-privacidad');
	}
	public function gracias(){
		$this->load->view('gracias');
	}

	 public function NotFound(){
    $data['page'] = "notfound";
    $this->load->view('notfound');
  	}

	public function contacto()
  	{
  	$this->load->helper('url');
    $datos = $_POST;
    $this->validarDatos($datos);
    $this->enviarEmail($datos);
    header("location: https://mo47.com.mx/contacto/gracias");
    //redirect('gracias');
    //$this->load->view('/gracias');
  }

  private function enviarEmail($datos)
  {

    $mensaje = "<p>Este contacto viene de https://mo47.com.mx/ </p>";
    $mensaje .= "<ul>";
    $campos = array("nombre","ciudad", "telefono", "email", "mensaje", "utm_source", "utm_campaign", "utm_medium", "utm_term", "utm_content");
    foreach ($campos as $value) {
      $mensaje .= "<li>$value: $datos[$value]</li>";
    }
    $mensaje .= "</ul>";


    $mail = new PHPMailer(true);

    try {
      $mail->isSMTP();                                      // Set mailer to use SMTP
      $mail->Host = 'smtp.office365.com';                   // Specify main and backup SMTP servers
      $mail->SMTPAuth = true;                               // Enable SMTP authentication
      $mail->Username = 'ejemplo@ejemplo.com';              // SMTP username
      $mail->Password = 'MO.47.Condesa';                           // SMTP password
      $mail->SMTPSecure = 'TLS';                            // Enable TLS encryption, `ssl` also accepted
      $mail->Port = 587;

      //**//
      $mail->setFrom('ejemplo@ejemplo.com', 'Nuevo contacto  FRM Montes de oca');
      //develop;

      //production
      $mail->addAddress('ejemplo@ejemplo.com');
      $mail->addCC('andreemalerva@gmail.com');

      $mail->Subject  = "Nuevo contacto  FRM Montes de oca";
      $mail->Body = $mensaje;
      $mail->IsHTML(true);
      // Activo condificacción utf-8
      $mail->CharSet = 'UTF-8';

      /**/
      $mail->send();

      //**//
      return true;

    } catch (Exception $exception) {
      return false;
    }
  }
  /*private function enviarEmail($datos)
  {
    $configMail =  array(
      'charset' => 'utf-8',
      'mailtype' => 'html'
    );
    $mensaje = "<p>Este contacto viene de https://mo47.com.mx/ </p>";
    $mensaje .= "<ul>";
    $campos = array("nombre", "ciudad", "telefono", "email");
    foreach ($campos as $value) {
      $mensaje .= "<li>$value: $datos[$value]</li>";
    }
    $mensaje .= "</ul>";

    $this->email->initialize($configMail);
    $this->email->from('noreply@mo47.com.mx', 'Notificacion mo47');
    $this->email->to('mailto:ejemplo@ejemplo.com');
    $this->email->subject("Nuevo lead frm-mo47");
    $this->email->message($mensaje);
    $this->email->send();

  }*/
  public function debugMail()
  {

    $mail = new PHPMailer(true);

    try {
      $mail->SMTPDebug = 2;
      $mail->isSMTP();                                      // Set mailer to use SMTP
      $mail->Host = 'smtp.office365.com';                   // Specify main and backup SMTP servers
      $mail->SMTPAuth = true;                               // Enable SMTP authentication
      $mail->Username = 'ejemplo@ejemplo.com';              // SMTP username
      $mail->Password = 'MO.47.Condesa';                           // SMTP password
      $mail->SMTPSecure = 'TLS';                            // Enable TLS encryption, `ssl` also accepted
      $mail->Port = 587;

      //**//
      $mail->setFrom('ejemplo@ejemplo.com', 'Nuevo contacto frm-Montes de oca');
      //develop;
      $mail->addAddress('andreemalerva@gmail.com');
      //production

      $mail->Subject  = "Solicitud de contacto frm-Montes de oca";
      $mail->Body = "esto es una prueba";
      $mail->IsHTML(true);
      // Activo condificacciÃ³n utf-8
      $mail->CharSet = 'UTF-8';

      /**/
      $mail->send();

      //**//

      return true;
    } catch (Exception $exception) {
      return false;
    }
  }
  private function validarDatos($datos)
  {
    $this->load->library('session');
    $datos = $this->limpiarDatos($datos);
    $old = array();
    $error = array();


    if ($datos['nombre'] == '') {
      array_push($error, '• el nombre es requerido');
      $old['nombre'] = $datos['nombre'];
    } elseif (strlen($datos['nombre']) < 3) {
      array_push($error, '• el nombre debe ser de al menos 3 caracteres');
      $old['nombre'] = $datos['nombre'];
    } else {
      $old['nombre'] = $datos['nombre'];
    }
    
    if ($datos['ciudad'] == '') {
      array_push($error, '• La ciudad es requerido');
      $old['nombre'] = $datos['ciudad'];
    } elseif (strlen($datos['ciudad']) < 3) {
      array_push($error, '• La ciudad debe ser de al menos 3 caracteres');
      $old['ciudad'] = $datos['ciudad'];
    } else {
      $old['ciudad'] = $datos['ciudad'];
    }



    if ($datos['telefono'] == '') {
      array_push($error, '• el teléfono es requerido');
      $old['telefono'] = $datos['telefono'];
    } elseif (strlen($datos['telefono']) < 10) {
      array_push($error, '• el telefono debe tener al menos 10 caracteres');
      $old['telefono'] = $datos['telefono'];
    } else {
      $old['telefono'] = $datos['telefono'];
    }

    if ($datos['email'] == '') {
      array_push($error, '• el correo electrónico es requerido');
      $old['email'] = $datos['email'];
    } elseif (!filter_var($datos['email'], FILTER_VALIDATE_EMAIL)) {
      array_push($error, '• el correo no es valido');
      $old['email'] = $datos['email'];
    } else {
      $old['email'] = $datos['email'];
    }


    if (!empty($error)) {
      $this->session->set_flashdata('error', $error);
      $this->session->set_flashdata('old', $old);
      redirect('index');
    }
  }
  private function limpiarDatos($datos)
  {
    foreach ($datos as $campo => $valor) {

      $datos[$campo] = htmlspecialchars($valor, ENT_QUOTES);
      $datos[$campo] = trim($valor);
    }
    return $datos;
  }
}
