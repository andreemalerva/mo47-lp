<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" href="assets/img/favicon.svg" sizes="32x32" />
  <title>Montes de Oca</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous" />
  <link rel="stylesheet" href="assets/css/gracias.css?v=<?= rand() ?>" />
      <!-- Global site tag (gtag.js) - Google Ads: 470694287 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-470694287"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'AW-470694287');
    </script>
  <!-- Event snippet for Contacto conversion page -->
  <script>
    gtag('event', 'conversion', {
      'send_to': 'AW-470694287/8CAoCL35qOwBEI_zuOAB'
    });
  </script>
  <!-- Facebook Pixel Code -->
  <script>
    ! function(f, b, e, v, n, t, s) {
      if (f.fbq) return;
      n = f.fbq = function() {
        n.callMethod ?
          n.callMethod.apply(n, arguments) : n.queue.push(arguments)
      };
      if (!f._fbq) f._fbq = n;
      n.push = n;
      n.loaded = !0;
      n.version = '2.0';
      n.queue = [];
      t = b.createElement(e);
      t.async = !0;
      t.src = v;
      s = b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
      'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '227428632223619');
    fbq('track', 'PageView');
    fbq('track', 'Lead');
  </script>
  <noscript>
    <img height="1" width="1" src="https://www.facebook.com/tr?id=227428632223619&ev=PageView
&noscript=1" />
  </noscript>
  <!-- End Facebook Pixel Code -->
</head>

<body>
  <main>
    <nav class="navbar navbares justify-content-center">
      <span class="navbar-text">
        <img src="assets/img/Logo.svg" alt="logotipo Joaquina">
      </span>
    </nav>
    <div class="container-fluid container-margen">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 ">
          <p class="text-principal-gracias">
            ¡Gracias por tu interés!
          </p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 ">
          <p class="text-sub-gracias">
            Hemos recibido tus datos, próximamente nos pondremos en contacto contigo para brindarte más detalles y ayudarte a elegir el depa ideal.

          </p>
        </div>
      </div>

    </div>

  </main>


</body>

</html>