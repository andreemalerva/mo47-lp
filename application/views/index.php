<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="assets/img/favicon.svg" sizes="32x32" />
    <title>Montes de Oca</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous" />
    <link rel="stylesheet" href="assets/css/main.css?v=<?= rand() ?>" />
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MSJKMDN');</script>
<!-- End Google Tag Manager -->
    <!-- Global site tag (gtag.js) - Google Ads: 470694287 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-470694287"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'AW-470694287');
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-184742548-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-184742548-1');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '227428632223619');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" src="https://www.facebook.com/tr?id=227428632223619&ev=PageView
&noscript=1" />
    </noscript>
    <!-- End Facebook Pixel Code -->
    
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MSJKMDN"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

 <!-- Código de instalación Cliengo para https://mo47.com.mx/contacto/ --> <script type="text/javascript">(function () { var ldk = document.createElement('script'); ldk.type = 'text/javascript'; ldk.async = true; ldk.src = 'https://s.cliengo.com/weboptimizer/5fdd3c49e029b3002ab40ce7/5fdd3c51a5120c002ae96d1f.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ldk, s); })();</script>
 <section id="principal">
        <div class="conten">

            <nav class="navbar navbares justify-content-center movil-none">
                <span class="navbar-text">
                    <img src="assets/img/Logo.svg" alt="logotipo Montes de oca">
                </span>
            </nav>
            <nav class="navbar navbares-1 justify-content-center desktop-none">
                <span class="navbar-text">
                    <img src="assets/img/Logo.svg" class="logo-text" alt="logotipo Montes de Oca">
                </span>
            </nav>

            <section class="container" id="hero">
                <div class="position-hero">
                    <img src="assets/img/form-1.png" class="tam-form-1" alt="forma-hero">
                </div>
                <div class="contenedor hi">

                    <div class="row hi">
                        <div class="col-lg-6 col-md-6 col-sm-12 d-flex align-items-center">
                            <div class="hero-content-details text-left">
                                <h1 class="hero-title">Departamentos en renta, totalmente amueblados
                                    desde $24,200 MXN.</h1>
                                <p class="hero-text">
                                    ¡Disfruta, descubre, vive Condesa!
                                </p>
                                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <div class="carousel-item active ">
                                            <img class="d-block w-100 img-3" src="assets/img/carrousel/mo47-02.jpg" alt="First slide">

                                            <div class="salon">Renta sin aval o fianza
                                                <div class="position-form-2">
                                                    <img src="assets/img/form-4.svg" class="img-form-2" alt="forma-hero">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="carousel-item">
                                            <img class="d-block w-100 img-3" src="assets/img/carrousel/mo47-03.jpg" alt="Second slide">

                                            <div class="salon">Solo te toma 15 minutos
                                                <div class="position-form-2">
                                                    <img src="assets/img/form-4.svg" class="img-form-2" alt="forma-hero">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="carousel-item">
                                            <img class="d-block w-100 img-3" src="assets/img/carrousel/mo47-01.jpg" alt="Third slide">

                                            <div class="salon">Pet friendly
                                                <div class="position-form-2">
                                                    <img src="assets/img/form-4.svg" class="img-form-2" alt="forma-hero">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <ol class="carousel-indicators">
                                        <li id="carr_1" data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                        <li id="carr_2" data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                        <li id="carr_3" data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                    </ol>
                                </div>

                                <p class="hero-text-p">
                                    Servicios incluídos. <br>
                                    Cualquier situación inesperada en tu departamento, te ayudamos a resolverlo.
                                </p>

                                <button id="info_banner" type="button" class="btn boton-card desktop-none scroll" data-mobile="#formu">
                                    MÁS INFORMACIÓN </button>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 d-none d-flex align-items-end display-f">
                            <div class="hero-form-content">
                                <form method="POST" class="row" id="formulario-hero" action="contacto">
                                    <div class="col-12 form-text">
                                        <h3 class="hero-form-title">
                                            Tu departamento<br>
                                            te está esperando
                                        </h3>
                                    </div>
                                    <div class="col-12 form-group">
                                        <input type="text" name="nombre" id="nombre" placeholder="Nombre:" class="ctr-h" required />
                                    </div>
                                    <div class="col-12 form-group">
                                        <input type="text" name="ciudad" id="ciudad" placeholder="Ciudad:" class="ctr-h" required />
                                    </div>
                                    <div class="col-12 form-group">
                                        <input type="mail" name="email" id="email" placeholder="Correo:" class="ctr-h" required />
                                    </div>
                                    <div class="col-12 form-group">
                                        <input type="tel" name="telefono" id="telefono" placeholder="Teléfono:" class="ctr-h" required />
                                    </div>
                                    <div class="col-12 form-group">
                                        <textarea name="mensaje" id="mensaje" placeholder="Deja un comentario:" class="ctr-h"></textarea>
                                    </div>
                                    <div>
                                        <input type="hidden" name="utm_source" id="utm_source">
                                        <input type="hidden" name="utm_content" id="utm_content">
                                        <input type="hidden" name="utm_campaign" id="utm_campaign">
                                        <input type="hidden" name="utm_term" id="utm_term">
                                        <input type="hidden" name="utm_medium" id="utm_medium">
                                    </div>
                                    <div class="col-12 text-center">
                                        <button type="submit" class="button-red button-form" id="enviar">
                                            MÁS INFORMACIÓN </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section>
                <div class="row navbares-sec2">
                    <div class="col-lg-12 col-md-12 col-sm-12 text-right">
                        <img src="assets/img/form-3.svg" alt="">
                    </div>
                </div>
            </section>

            <section id="cards">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-title-cards d-none d-sm-none d-md-block">
                            Nuestros departamentos
                        </div>
                        <div class="container">
                            <div class="d-none d-sm-none d-md-block">
                                <!-- <div class="wrapper">
                                    <div class="columna">
                                        <div class="card text-center">
                                            <img class="card-img-top img-tam-card" src="assets/img/cards/img1.jpg" alt="Card image cap">
                                            <div class="card-body">
                                                <h5 class="card-title titulo-card">Interior</h5>
                                                <br>
                                                <br>
                                                <p class="card-text parrafo-card">Con 2 recámaras, cocina-comedor y baño.
                                                    <br>
                                                    <br>
                                                </p>
                                                <p class="parrafo-card">
                                                    <b>Desde $24,200 mxn</b>
                                                </p>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="columna-2">
                                        <div class="card text-center">
                                            <img class="card-img-top img-tam-card" src="assets/img/cards/img2.jpg" alt="Card image cap">
                                            <div class="card-body">
                                                <h5 class="card-title titulo-card">Superior con balcón</h5>
                                                <br>
                                                <br>
                                                <p class="card-text parrafo-card">TIpo loft con 1 recámara, baño, sala-comedor y estudio.
                                                    <br>
                                                    <br>
                                                </p>
                                                <p class="parrafo-card">
                                                    <b>Desde $25,200 mxn</b>
                                                </p>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="columna-3">
                                        <div class="card text-center">
                                            <img class="card-img-top img-tam-card" src="assets/img/cards/img3.jpg" alt="Card image cap">
                                            <div class="card-body">
                                                <h5 class="card-title titulo-card">Exterior con balcón</h5>
                                                <br>
                                                <br>
                                                <p class="card-text parrafo-card">Con 1 recámara, cocina completa, baño, estudio con daybed y escritorio.
                                                    <br>
                                                    <br>
                                                </p>
                                                <p class="parrafo-card">
                                                    <b>Desde $25,900 mxn</b>
                                                </p>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="columna-4">
                                        <div class="card text-center">
                                            <img class="card-img-top img-tam-card" src="assets/img/cards/img4.jpg" alt="Card image cap">
                                            <div class="card-body">
                                                <h5 class="card-title titulo-card">Exterior</h5>
                                                <br>
                                                <br>
                                                <p class="card-text parrafo-card">Con 1 recámara, baño, cocina completa y sala-comedor.
                                                    <br>
                                                    <br>
                                                </p>
                                                <p class="parrafo-card">
                                                    <b>Desde $25,900 mxn</b>
                                                </p>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="columna-5">
                                        <div class="card text-center">
                                            <img class="card-img-top img-tam-card" src="assets/img/cards/img5.jpg" alt="Card image cap">
                                            <div class="card-body">
                                                <h5 class="card-title titulo-card">Exterior 2 recámaras</h5>
                                                <br>
                                                <br>
                                                <p class="card-text parrafo-card">Ubicado en planta baja, con sala-comedor, baño y dos recámaras.
                                                    <br>
                                                    <br>
                                                </p>
                                                <p class="parrafo-card">
                                                    <b>Desde $33,800 mxn</b>
                                                </p>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="wrap">
                                    <div class="">
                                        <div class="card text-center">
                                            <div class="img-cards-1 img-tam-card"></div>
                                            <!-- <img class="card-img-top img-tam-card" src="assets/img/cards/img1.jpg" alt="Card image cap"> -->
                                            <div class="card-body">
                                                <h5 class="card-title titulo-card">Interior</h5>
                                                <br>
                                                <br>
                                                <p class="card-text parrafo-card">Con 2 recámaras, cocina-comedor y baño.
                                                    <br>
                                                    <br>
                                                </p>
                                                <p class="parrafo-card">
                                                    <b>Desde $24,200 mxn</b>
                                                </p>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="">
                                        <div class="card text-center">
                                            <div class="img-cards-2 img-tam-card"></div>
                                            <!-- <img class="card-img-top img-tam-card" src="assets/img/cards/img2.jpg" alt="Card image cap"> -->
                                            <div class="card-body">
                                                <h5 class="card-title titulo-card">Superior con balcón</h5>
                                                <br>
                                                <br>
                                                <p class="card-text parrafo-card">TIpo loft con 1 recámara, baño, sala-comedor y estudio.
                                                    <br>
                                                    <br>
                                                </p>
                                                <p class="parrafo-card">
                                                    <b>Desde $25,200 mxn</b>
                                                </p>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="">
                                        <div class="card text-center">
                                            <div class="img-cards-3 img-tam-card"></div>

                                            <!-- <img class="card-img-top img-tam-card" src="assets/img/cards/img3.jpg" alt="Card image cap"> -->
                                            <div class="card-body">
                                                <h5 class="card-title titulo-card">Exterior con balcón</h5>
                                                <br>
                                                <br>
                                                <p class="card-text parrafo-card">Con 1 recámara, cocina completa, baño, estudio con daybed y escritorio.
                                                    <br>
                                                    <br>
                                                </p>
                                                <p class="parrafo-card">
                                                    <b>Desde $25,900 mxn</b>
                                                </p>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="">
                                        <div class="card text-center">
                                            <div class="img-cards-4 img-tam-card"></div>

                                            <!-- <img class="card-img-top img-tam-card" src="assets/img/cards/img4.jpg" alt="Card image cap"> -->
                                            <div class="card-body">
                                                <h5 class="card-title titulo-card">Exterior</h5>
                                                <br>
                                                <br>
                                                <p class="card-text parrafo-card">Con 1 recámara, baño, cocina completa y sala-comedor.
                                                    <br>
                                                    <br>
                                                </p>
                                                <p class="parrafo-card">
                                                    <b>Desde $25,900 mxn</b>
                                                </p>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="">
                                        <div class="card text-center">
                                            <div class="img-cards-5 img-tam-card"></div>

                                            <!-- <img class="card-img-top img-tam-card" src="assets/img/cards/img5.jpg" alt="Card image cap"> -->
                                            <div class="card-body">
                                                <h5 class="card-title titulo-card">Exterior 2 recámaras</h5>
                                                <br>
                                                <br>
                                                <p class="card-text parrafo-card">Ubicado en planta baja, con sala-comedor, baño y dos recámaras.
                                                    <br>
                                                    <br>
                                                </p>
                                                <p class="parrafo-card">
                                                    <b>Desde $33,800 mxn</b>
                                                </p>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row d-block d-sm-block d-md-none">
                                <div class="col-12 text-title-cards">
                                    Nuestros departamentos
                                </div>
                                <div class="col-12">
                                    <div class="card text-center">
                                        <div class="img-cards-1 img-tam-card"></div>
                                        <!-- <img class="card-img-top img-tam-card" src="assets/img/cards/img1.jpg" alt="Card image cap"> -->
                                        <div class="card-body">
                                            <h5 class="card-title titulo-card">Interior</h5>
                                            <br>
                                            <br>
                                            <p class="card-text parrafo-card">Con 2 recámaras, cocina-comedor y baño.
                                                <br>
                                                <br>
                                            </p>
                                            <p class="parrafo-card">
                                                <b>Desde $24,200 mxn</b>
                                            </p>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="card text-center">
                                        <div class="img-cards-2 img-tam-card"></div>
                                        <!-- <img class="card-img-top img-tam-card" src="assets/img/cards/img2.jpg" alt="Card image cap"> -->
                                        <div class="card-body">
                                            <h5 class="card-title titulo-card">Superior con balcón</h5>
                                            <br>
                                            <br>
                                            <p class="card-text parrafo-card">TIpo loft con 1 recámara, baño, sala-comedor y estudio.
                                                <br>
                                                <br>
                                            </p>
                                            <p class="parrafo-card">
                                                <b>Desde $25,200 mxn</b>
                                            </p>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="card text-center">
                                        <div class="img-cards-3 img-tam-card"></div>
                                        <!-- <img class="card-img-top img-tam-card" src="assets/img/cards/img3.jpg" alt="Card image cap"> -->
                                        <div class="card-body">
                                            <h5 class="card-title titulo-card">Exterior con balcón</h5>
                                            <br>
                                            <br>
                                            <p class="card-text parrafo-card">Con 1 recámara, cocina completa, baño, estudio con daybed y escritorio.
                                                <br>
                                                <br>
                                            </p>
                                            <p class="parrafo-card">
                                                <b>Desde $25,900 mxn</b>
                                            </p>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="card text-center">
                                        <div class="img-cards-4 img-tam-card"></div>
                                        <!-- <img class="card-img-top img-tam-card" src="assets/img/cards/img4.jpg" alt="Card image cap"> -->
                                        <div class="card-body">
                                            <h5 class="card-title titulo-card">Exterior</h5>
                                            <br>
                                            <br>
                                            <p class="card-text parrafo-card">Con 1 recámara, baño, cocina completa y sala-comedor.
                                                <br>
                                                <br>
                                            </p>
                                            <p class="parrafo-card">
                                                <b>Desde $25,900 mxn</b>
                                            </p>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="card text-center">
                                        <div class="img-cards-5 img-tam-card"></div>
                                        <!-- <img class="card-img-top img-tam-card" src="assets/img/cards/img5.jpg" alt="Card image cap"> -->
                                        <div class="card-body">
                                            <h5 class="card-title titulo-card">Exterior 2 recámaras</h5>
                                            <br>
                                            <br>
                                            <p class="card-text parrafo-card">Ubicado en planta baja, con sala-comedor, baño y dos recámaras.
                                                <br>
                                                <br>
                                            </p>
                                            <p class="parrafo-card">
                                                <b>Desde $33,800 mxn</b>
                                            </p>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row navbares-botton-cards">
                        <div class="col-lg-12 col-md-12 col-sm-12 justify-content-center">
                            <button type="button" id="cotizar"class="button-card button-form scroll" data-desktop="#formulario-hero" data-mobile="#formu">COTIZAR</button>
                        </div>
                    </div>
                </div>

            </section>
            <section>
                <div class="container sin">
                    <div class="row movil-none">
                        <div class="col-lg-6 col-md-6 img-padding">
                            <img src="assets/img/img1.jpg" class="img-tam-1" alt="imagen1">
                        </div>
                        <div class="col-lg-6 col-md-6 justify-content-center">
                            <div class="position-form-3">
                                <img src="assets/img/form-4.svg" class="img-form-2" alt="forma disfruta">
                            </div>
                            <div class="caja-text-1">
                                <div class="text-title-caja1">
                                    Disfruta la Condesa
                                </div>
                                <p class="parrafo-caja1">
                                    Admira la belleza de la ciudad desde el rooftop</p>
                                <button id="detalles"type="submit" class="button-detalles button-img scroll" data-desktop="#formulario-hero" id=" enviar">
                                    MÁS DETALLES </button>
                            </div>

                        </div>

                        <div class="col-lg-6 col-md-6 img-padding color-font">
                            <div class="position-form-4">
                                <img src="assets/img/form-5.svg" class="img-form-4" alt="forma 4">
                            </div>
                            <div class="caja-text-2">
                                <div class="text-title-caja2">
                                    <b>MO47,</b><br>
                                    un edificio cOmo ninguno.
                                </div>
                                <p class="parrafo-caja2">
                                    Construido por el Arq. Abraham Zabludovsky en 1954, perteneciente al movimiento moderno y cuidadosamente restaurado respetando los materiales y acabados originales. VGZ arquitectura han seleccionado una colección
                                    de muebles, textiles y objetos de
                                    diseñadores mexicanos contemporáneos.
                                    <br>
                                    <br>
                                    Un concepto que una la modernidad y lo clásico. </p>
                                <button id="info_body" type="submit" class="button-edificio button-img scroll" data-desktop="#formulario-hero" id=" enviar">
                                    MÁS INFORMACIÓN</button>
                                <div class="position-form-6">
                                    <img src="assets/img/form-6.svg" class="img-form-6" alt="forma 4">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 img-padding">
                            <img src="assets/img/img2.jpg" class="img-tam-2" alt="imagen2">
                        </div>
                    </div>
                    <div class="row desktop-none">
                        <div class="col-lg-6 col-md-6 img-padding">
                            <img src="assets/img/img1.jpg" class="img-tam-1" alt="imagen1">
                        </div>
                        <div class="col-lg-6 col-md-6 justify-content-center">
                            <div class="position-form-3">
                                <img src="assets/img/form-4.svg" class="img-form-2" alt="forma disfruta">
                            </div>
                            <div class="caja-text-1">
                                <div class="text-title-caja1">
                                    Disfruta la Condesa
                                </div>
                                <p class="parrafo-caja1">
                                    Admira la belleza de la ciudad desde el rooftop</p>
                                <button id="detalles-m" type="submit" class="button-detalles button-form scroll" data-mobile="#formu">
                                    MÁS DETALLES </button>
                            </div>

                        </div>


                        <div class="col-lg-6 col-md-6 img-padding">
                            <img src="assets/img/img2.jpg" class="img-tam-2" alt="imagen2">
                        </div>
                        <div class="col-lg-6 col-md-6 img-padding color-font">
                            <div class="caja-text-2">
                                <div class="text-title-caja2">
                                    <b>MO47,</b><br>
                                    un edificio cOmo ninguno.
                                </div>
                                <p class="parrafo-caja2">
                                    Construido por el Arq. Abraham Zabludovsky en 1954, perteneciente al movimiento moderno y cuidadosamente restaurado respetando los materiales y acabados originales. VGZ arquitectura han seleccionado una colección
                                    de muebles, textiles y objetos de
                                    diseñadores mexicanos contemporáneos.
                                    <br>
                                    <br>
                                    Un concepto que una la modernidad y lo clásico. </p>
                                <button id="info_body-m" type="submit" class="button-edificio button-form scroll" data-mobile="#formu">
                                    MÁS INFORMACIÓN</button>

                            </div>
                        </div>
                    </div>
                </div>

            </section>

            <section id="ubicacion">
                <div class="d-block d-sm-block d-md-none">
                    <div class="container">
                        <div class="text-center form-m">
                            <form class="row" id="formu" method="POST" action="contacto">

                                <div class="col-12 form-group">
                                    <h3 class="hero-form-title">
                                        Tu departamento
                                        te está esperando </h3>
                                </div>
                                <div class="col-12 form-group">
                                    <input type="text" name="nombre" id="nombre" placeholder="Nombre:" class="ctr-h" required />
                                </div>
                                <div class="col-12 form-group">
                                    <input type="text" name="ciudad" id="ciudad" placeholder="Ciudad:" class=" ctr-h" required />
                                </div>
                                <div class="col-12 form-group">
                                    <input type="tel" name="telefono" id="telefono" placeholder="Teléfono:" class="ctr-h" required />
                                </div>
                                <div class="col-12 form-group">
                                    <input type="mail" name="email" id="email" placeholder="Correo:" class="ctr-h" required />
                                </div>
                                <div class="col-12 form-group">
                                    <textarea name="mensaje" id="mensaje" placeholder="Deja un comentario:" class="ctr-h"></textarea>
                                </div>
                                <div>
                                    <input type="hidden" id="utm_campaign-m" name="utm_campaign">
                                    <input type="hidden" id="utm_source-m" name="utm_source">
                                    <input type="hidden" id="utm_medium-m" name="utm_medium">
                                    <input type="hidden" id="utm_term-m" name="utm_term">
                                    <input type="hidden" id="utm_content-m" name="utm_content">
                                </div>
                                <div class="col-12 form-group text-center">
                                    <button type="submit" class="button-red button-form" id="enviar-m">
                                        MÁS INFOMACIÓN </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row color movil-none">
                        <div class="col-lg-4 col-md-4 col-sm-12 ">
                            <div class="back-ubicacion">
                                <div class="caja-ubicacion">
                                    <div class="text-title-ubicacion">
                                        VIVE EN UNA DE
                                        LAS ZONAS CON MÁS OFERTA GASTRONÓMICA, CULTURAL Y DE ENTRETENIMIENTO
                                    </div>
                                    <p class="parrafo-ubicacion">
                                        Fernando Montes de Oca 47, Col. Condesa, CDMX.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-12 margenes">
                            
                            <iframe class="iframe" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=es&amp;q=Fernando%20Montes%20de%20Oca%2047,%20Colonia%20condesa+(MO47)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed">
                            </iframe>

                        </div>
                    </div>
                    <div class="row color desktop-none">

                        <div class="col-lg-8 col-md-8 col-sm-12 margenes">
                            <iframe class="iframe" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=es&amp;q=Fernando%20Montes%20de%20Oca%2047,%20Colonia%20condesa+(MO47)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed">
                            </iframe>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 ">
                            <div class="back-ubicacion">
                                <div class="caja-ubicacion">
                                    <div class="text-title-ubicacion">
                                        VIVE EN UNA DE
                                        LAS ZONAS CON MÁS OFERTA GASTRONÓMICA, CULTURAL Y DE ENTRETENIMIENTO
                                    </div>
                                    <p class="parrafo-ubicacion">
                                        Fernando Montes de Oca 47, Col. Condesa, CDMX.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </section>
            <section id=aviso_privacidad>
                <div class="row navbar-aviso">
                    <div class="col-lg-12 col-md-12 col-sm-12 d-flex justify-content-center">
                        <a href="aviso-de-privacidad/" class="text-aviso">Aviso de privacidad</a>
                    </div>
                </div>
            </section>
        </div>
    </section>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- The core Firebase JS SDK is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/7.21.0/firebase-app.js"></script>

    <!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
    <script src="https://www.gstatic.com/firebasejs/7.21.0/firebase-analytics.js"></script>

    <script src="https://www.gstatic.com/firebasejs/7.21.0/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/7.21.0/firebase-database.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js"></script>

    <script src="assets/js/form.js?v=<?= rand() ?>"></script>
    <script>
        $(function() {
            let scroll_link = $('.scroll');

            //smooth scrolling -----------------------
            scroll_link.click(function(e) {

                e.preventDefault();
                /**/
                if (window.matchMedia("(max-width: 1024px)").matches) {

                    gotoFrm = $(this).data('mobile')
                } else {

                    gotoFrm = $(this).data('desktop');
                }
                /**/

                let url = $('body').find(gotoFrm).offset().top;
                $('html, body').animate({
                    scrollTop: url
                }, 700);
                $(this).parent().addClass('active');
                $(this).parent().siblings().removeClass('active');
                return false;
            });
        });
    </script>
</body>

</html>